# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 17:10:39 2018

@author: james.chan
"""

import numpy as np
import matplotlib.pyplot as plt
import h5py
import scipy
from PIL import Image
from scipy import ndimage
from lr_utils import load_dataset



##%matplotlib inline
#
## Loading the data (cat/non-cat)
#train_set_x_orig, train_set_y, test_set_x_orig, test_set_y, classes = load_dataset()
#
#### START CODE HERE ### (≈ 3 lines of code)
#m_train = train_set_x_orig.shape[0]
#m_test = test_set_x_orig.shape[0]
#num_px = train_set_x_orig.shape[1]
#### END CODE HERE ###
#
#print ("Number of training examples: m_train = " + str(m_train))
#print ("Number of testing examples: m_test = " + str(m_test))
#print ("Height/Width of each image: num_px = " + str(num_px))
#print ("Each image is of size: (" + str(num_px) + ", " + str(num_px) + ", 3)")
#print ("train_set_x shape: " + str(train_set_x_orig.shape))
#print ("train_set_y shape: " + str(train_set_y.shape))
#print ("test_set_x shape: " + str(test_set_x_orig.shape))
#print ("test_set_y shape: " + str(test_set_y.shape))
#
## Reshape the training and test examples
#
#### START CODE HERE ### (≈ 2 lines of code)
#train_set_x_flatten = train_set_x_orig.reshape(train_set_x_orig.shape[0], -1).T 
#test_set_x_flatten = test_set_x_orig.reshape(test_set_x_orig.shape[0], -1).T 
#### END CODE HERE ###
#
#print ("train_set_x_flatten shape: " + str(train_set_x_flatten.shape))
#print ("train_set_y shape: " + str(train_set_y.shape))
#print ("test_set_x_flatten shape: " + str(test_set_x_flatten.shape))
#print ("test_set_y shape: " + str(test_set_y.shape))
#print ("sanity check after reshaping: " + str(train_set_x_flatten[0:5,0]))
#
## standardize our dataset.
#train_set_x = train_set_x_flatten/255.
#test_set_x = test_set_x_flatten/255.
#
## 4. Building the parts of our algorithm
#
## The main steps for building a Neural Network are:
#
##Define the model structure (such as number of input features)
##Initialize the model's parameters
##Loop:
##Calculate current loss (forward propagation)
##Calculate current gradient (backward propagation)
##Update parameters (gradient descent)
#    
## 4.1 - Helper functions
## GRADED FUNCTION: sigmoid
def sigmoid(z):
    """
    Compute the sigmoid of z

    Arguments:
    z -- A scalar or numpy array of any size.

    Return:
    s -- sigmoid(z)
    """

    ### START CODE HERE ### (≈ 1 line of code)
    s = (1 / (1 + np.exp(-z)))
    ### END CODE HERE ###
    
    return s    

#print ("sigmoid([0, 2]) = " + str(sigmoid(np.array([0,2]))))
#
## 4.2 - Initializing parameters
## GRADED FUNCTION: initialize_with_zeros
#def initialize_with_zeros(dim):
#    """
#    This function creates a vector of zeros of shape (dim, 1) for w and initializes b to 0.
#    
#    Argument:
#    dim -- size of the w vector we want (or number of parameters in this case)
#    
#    Returns:
#    w -- initialized vector of shape (dim, 1)
#    b -- initialized scalar (corresponds to the bias)
#    """
#    
#    ### START CODE HERE ### (≈ 1 line of code)
#    w = np.zeros((dim, 1))
#    b = 0
#    ### END CODE HERE ###
#
#    assert(w.shape == (dim, 1))
#    assert(isinstance(b, float) or isinstance(b, int))
#    
#    return w, b
#
#dim = 2
#w, b = initialize_with_zeros(dim)
#print ("w = " + str(w))
#print ("b = " + str(b))
#
## 4.3 - Forward and Backward propagation
##Hints:
##Forward Propagation:
##You get X
##You compute  A=σ(wTX+b)=(a(1),a(2),...,a(m−1),a(m)) 
##You calculate the cost function:  J=−1/m∑m i=1 y(i)log(a(i)) + (1−y(i))log(1−a(i))
## GRADED FUNCTION: propagate
def propagate(w, b, X, Y):
    """
    Implement the cost function and its gradient for the propagation explained above

    Arguments:
    w -- weights, a numpy array of size (num_px * num_px * 3, 1)
    b -- bias, a scalar
    X -- data of size (num_px * num_px * 3, number of examples)
    Y -- true "label" vector (containing 0 if non-cat, 1 if cat) of size (1, number of examples)

    Return:
    cost -- negative log-likelihood cost for logistic regression
    dw -- gradient of the loss with respect to w, thus same shape as w
    db -- gradient of the loss with respect to b, thus same shape as b
    
    Tips:
    - Write your code step by step for the propagation. np.log(), np.dot()
    """
    print('w.shape',w.shape)
    print('X.shape',X.shape)
    print('Y.shape',Y.shape)
    
    m = X.shape[1]
    print('m=',m)
    # FORWARD PROPAGATION (FROM X TO COST)
    ### START CODE HERE ### (≈ 2 lines of code)
    A = sigmoid(w.T.dot(X) + b)                                    # compute activation
    cost = -1/m * np.sum(np.dot(np.log(A),Y.T) + np.dot(np.log(1-A), (1-Y).T))                                # compute cost
#    ### END CODE HERE ###
#    
#    # BACKWARD PROPAGATION (TO FIND GRAD)
#    ### START CODE HERE ### (≈ 2 lines of code)
    dw = 1/m * np.dot(X, (A - Y).T)
    db = 1/m * np.sum(np.subtract(A, Y))
#    ### END CODE HERE ###
#
    assert(dw.shape == w.shape)
    assert(db.dtype == float)
    print ("cost1 = ",cost)
    cost = np.squeeze(cost)
    print ("cost2 = ",cost)
    assert(cost.shape == ())
#    
    grads = {"dw": dw,
             "db": db}
#    
    return grads, cost
#    return cost

w = np.array([[1.],[2.]])
b= 2.
X = np.array([[1.,2.,-1.],[3.,4.,-3.2]])
Y = np.array([[1,0,1]])
grads, cost = propagate(w, b, X, Y)
print ("dw = " + str(grads["dw"]))
print ("db = " + str(grads["db"]))
print ("cost = " + str(cost))